# gr-tutorial
Integrating UHD and fpga files into one directory

# Ошибка в UHD

Не страшная, но не приятная ошибка в драйвере UHD. Применить патч [uhd_mako.patch](uhd_mako.patch). Не генерируется верилоговский хедер. 

# Блок Gain

Простой блок усилителя. Просто знак умножения в коде. ID: DEADBEEF

Тактовые частоты блока: `rfnoc_chdr` и `rfnoc_ctrl`. 
Они подключены по умолчанию и их не указывать в `clk_domains`

## Цели в cmake

* `gnuradio-tutorial` - сборка блоков для гнурадио, make install
* `rfnoc_block_gain_tb` - тестбенч умножителя, требуется Vivado
* `gain_n310_rfnoc_image_core` - сборка ПЛИС с одним блоком Gain
* `gain2_n310_rfnoc_image_core` - сборка ПЛИС с двумя блоками Gain
* `gainip_n310_rfnoc_image_core` - сборка ПЛИС с блоком Gain и умножителем в виде `xci`

## Состав ПЛИС:

### Сборка gain_n310_rfnoc_image_core

[Прошивка](fpga_bit/gain_n310_rfnoc_image_core).

#### Блоки:

* 5 задейстрованных stream endpoint
* 2 не задействованных stream endpoint, возможно придется удалить
* 2 радио блока `radio_2x64.yml`
* 2 DUC (2 канала) `duc.yml`
* 2 DDC (2 канала) `ddc.yml`
* 1 gain блок `gain.yml`

#### Статические связи:

* ep0 -> duc0_0 -> radio0_0: TX
* radio0_0 -> ddc0_0 -> ep0: RX 
* ep1 -> duc0_1 -> radio0_1: TX
* radio0_1 -> ddc0_1 -> ep1: RX
* ep2 -> duc1_0 -> radio1_0: TX
* radio1_0 -> ddc1_0 -> ep2: RX
* ep3 -> duc1_1 -> radio1_1: TX
* radio1_1 -> ddc1_1 -> ep3: RX
* ep4 -> gain0
* gain0 -> ep4

####Занимаемые ресурсы:

**Slice Logic:**

|          Site Type         |  Used  | Fixed | Available | Util % |
| -------------------------- |  ----: | -----: | --------: | -----: |
| Slice LUTs                 | 144242 |     0 |    277400 | 52.00 |
| &nbsp;&nbsp;LUT as Logic             | 119871 |     0 |    277400 | 43.21 |
| &nbsp;&nbsp;LUT as Memory            |  24371 |     0 |    108200 | 22.52 |
| &nbsp;&nbsp;&nbsp;&nbsp;LUT as Distributed RAM |   7728 |     0 |           |       |
| &nbsp;&nbsp;&nbsp;&nbsp;LUT as Shift Register  |  16643 |     0 |           |       |
| Slice Registers            | 252577 |     2 |    554800 | 45.53 |
| &nbsp;&nbsp;Register as Flip Flop    | 252563 |     2 |    554800 | 45.52 |
| &nbsp;&nbsp;Register as Latch        |      0 |     0 |    554800 |  0.00 |
| &nbsp;&nbsp;Register as AND/OR       |     14 |     0 |    554800 | <0.01 |
| F7 Muxes                   |    832 |     0 |    138700 |  0.60 |
| F8 Muxes                   |    200 |     0 |     69350 |  0.29 |


**BRAM**

|     Site Type     |  Used | Available | Util % |
|-------------------|------:|----------:|------:|
| Block RAM Tile    | 518.5 |       755 | 68.68 |
| &nbsp;&nbsp;RAMB36/FIFO*    |   492 |       755 | 65.17 |
| &nbsp;&nbsp;&nbsp;&nbsp;RAMB36E1 only |   492 |           |       |
| &nbsp;&nbsp;RAMB18          |    53 |      1510 |  3.51 |
| &nbsp;&nbsp;&nbsp;&nbsp;RAMB18E1 only |    53 |           |       |


**DSP**

|    Site Type   | Used | Available | Util % |
|----------------|-----:|----------:|------:|
| DSPs           |  354 |      2020 | 17.52 |
| &nbsp;&nbsp;DSP48E1 only |  354 |           |       |


### Сборка gain2_n310_rfnoc_image_core

[Прошивка](fpga_bit/gain2_n310_rfnoc_image_core).

#### Блоки:

* 6 задейстрованных stream endpoint
* 2 радио блока `radio_2x64.yml`
* 2 DUC (2 канала) `duc.yml`
* 2 DDC (2 канала) `ddc.yml`
* 2 gain блок `gain.yml`

#### Статические связи:

* ep0 -> duc0_0 -> radio0_0: TX
* radio0_0 -> ddc0_0 -> ep0: RX
* ep1 -> duc0_1 -> radio0_1: TX
* radio0_1 -> ddc0_1 -> ep1: RX
* ep2 -> duc1_0 -> radio1_0: TX
* radio1_0 -> ddc1_0 -> ep2: RX
* ep3 -> duc1_1 -> radio1_1: TX
* radio1_1 -> ddc1_1 -> ep3: RX
* ep4 -> gain0
* gain0 -> ep4
* ep5 -> gain1
* gain1 -> ep5

#### Занимаемые ресурсы:

**Slice Logic:**

|          Site Type         |  Used  | Fixed | Available | Util % |
|----------------------------|-------:|------:|----------:|------:|
| Slice LUTs                 | 146271 |     0 |    277400 | 52.73 |
| &nbsp;&nbsp;LUT as Logic             | 120953 |     0 |    277400 | 43.60 |
| &nbsp;&nbsp;LUT as Memory            |  25318 |     0 |    108200 | 23.40 |
| &nbsp;&nbsp;&nbsp;&nbsp;LUT as Distributed RAM |   7872 |     0 |           |       |
| &nbsp;&nbsp;&nbsp;&nbsp;LUT as Shift Register  |  17446 |     0 |           |       |
| Slice Registers            | 253452 |     2 |    554800 | 45.68 |
| &nbsp;&nbsp;Register as Flip Flop    | 253438 |     2 |    554800 | 45.68 |
| &nbsp;&nbsp;Register as Latch        |      0 |     0 |    554800 |  0.00 |
| &nbsp;&nbsp;Register as AND/OR       |     14 |     0 |    554800 | <0.01 |
| F7 Muxes                   |   1408 |     0 |    138700 |  1.02 |
| F8 Muxes                   |    200 |     0 |     69350 |  0.29 |


**BRAM**

|     Site Type     |  Used | Fixed | Available | Util % |
|-------------------|------:|------:|----------:|------:|
| Block RAM Tile    | 566.5 |     0 |       755 | 75.03 |
| &nbsp;&nbsp;RAMB36/FIFO*    |   540 |     0 |       755 | 71.52 |
| &nbsp;&nbsp;&nbsp;&nbsp;RAMB36E1 only |   540 |       |           |       |
| &nbsp;&nbsp;RAMB18          |    53 |     0 |      1510 |  3.51 |
| &nbsp;&nbsp;&nbsp;&nbsp;RAMB18E1 only |    53 |       |           |       |


**DSP**

|    Site Type   | Used | Available | Util % |
|----------------|-----:|----------:|------:|
| DSPs           |  356 |      2020 | 17.62 |
| &nbsp;&nbsp;DSP48E1 only |  356 |           |       |


### Сборка gainip_n310_rfnoc_image_core

Блок реализует умножение 16-битных IQ отсчетов на 16 битный коэффициент умножения. 
Полученный 32-битный результат умножения клиппируется сверху и снизу (блок axi_clip_complex из соства UHD).   

[Прошивка](fpga_bit/gainip_n310_rfnoc_image_core).

### Особенности

Умножение сделано как IP блок комплексного умножителя, 
[https://www.xilinx.com/products/intellectual-property/complex_multiplier.html](https://www.xilinx.com/products/intellectual-property/complex_multiplier.html)
Verion 6.0.

IP создавался в отдельном проекте под необходимую ПЛИС (не важно под какую, всё равно при сборке перенастраивается).
Полученный `xci` файл копировался в `rfnoc/fpga/multiplier/multiplier.xci`. Имя создаваемого блока multiplier.

Настройки ![](rfnoc/fpga/ip/multiplier/multiplier_p1.png)

![](rfnoc/fpga/ip/multiplier/multiplier_p2.png)

Модифицировался файл [rfnoc/fpga/CMakeLists.txt](rfnoc/fpga/CMakeLists.txt), заметка NOTE.

Создавался [rfnoc/fpga/ip/multiplier/Makefile.inc](rfnoc/fpga/ip/multiplier/Makefile.inc)

Добавляем исходники IP блока для симуляции в [rfnoc/fpga/rfnoc_block_gainip/Makefile](rfnoc/fpga/rfnoc_block_gainip/Makefile).

XCI IP будет сгенерирован при симуляции в папке `rfnoc/fpga/rfnoc_block_<name>/build-ip`, а при синтезе в 
`<uhd>/fpga/usrp3/top/n3xx/build-ip/<fpga_name>/<ip_block_name>`.


#### Блоки

* 5 задейстрованных stream endpoint
* 2 радио блока `radio_2x64.yml`
* 2 DUC (2 канала) `duc.yml`
* 2 DDC (2 канала) `ddc.yml`
* 1 gainip блок `gainip.yml`

#### Статические связи:

* ep0 -> duc0_0 -> radio0_0: TX
* radio0_0 -> ddc0_0 -> ep0: RX
* ep1 -> duc0_1 -> radio0_1: TX
* radio0_1 -> ddc0_1 -> ep1: RX
* ep2 -> duc1_0 -> radio1_0: TX
* radio1_0 -> ddc1_0 -> ep2: RX
* ep3 -> duc1_1 -> radio1_1: TX
* radio1_1 -> ddc1_1 -> ep3: RX
* ep4 -> gainip0
* gainip0 -> ep4


#### Занимаемые ресурсы

**Slice Logic:**

|          Site Type         |  Used  | Available | Util% |
|----------------------------|-------:|----------:|------:|
| Slice LUTs                 | 137301 |    277400 | 49.50 |
| &nbsp;&nbsp;LUT as Logic             | 113213 |    277400 | 40.81 |
| &nbsp;&nbsp;LUT as Memory            |  24088 |    108200 | 22.26 |
| &nbsp;&nbsp;&nbsp;&nbsp;LUT as Distributed RAM |   7724 |           |       |
| &nbsp;&nbsp;&nbsp;&nbsp;LUT as Shift Register  |  16364 |           |       |
| Slice Registers            | 241149 |    554800 | 43.47 |
| &nbsp;&nbsp;Register as Flip Flop    | 241135 |    554800 | 43.46 |
| &nbsp;&nbsp;Register as Latch        |      0 |    554800 |  0.00 |
| &nbsp;&nbsp;Register as AND/OR       |     14 |    554800 | <0.01 |
| F7 Muxes                   |   1354 |    138700 |  0.98 |
| F8 Muxes                   |    199 |     69350 |  0.29 |


**BRAM**

|     Site Type     |  Used | Available | Util% |
|-------------------|------:|----------:|------:|
| Block RAM Tile    | 495.5 |       755 | 65.63 |
| &nbsp;&nbsp;RAMB36/FIFO*    |   470 |       755 | 62.25 |
| &nbsp;&nbsp;&nbsp;&nbsp;RAMB36E1 only |   470 |           |       |
| &nbsp;&nbsp;RAMB18          |    51 |      1510 |  3.38 |
| &nbsp;&nbsp;RAMB18E1 only |    51 |           |       |


**DSP**

|    Site Type   | Used | Available | Util% |
|----------------|-----:|----------:|------:|
| DSPs           |  356 |      2020 | 17.62 |
| &nbsp;&nbsp;DSP48E1 only |  356 |           |       |

