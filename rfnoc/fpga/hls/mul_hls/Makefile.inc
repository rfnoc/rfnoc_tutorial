

# ADD C/C++/tcl files
include $(TOOLS_DIR)/make/viv_hls_ip_builder.mak

HLS_IP_MUL_SRCS = $(addprefix $(IP_BUILD_DIR)/mul_hls/, \
mul_hls.cpp \
mul_hls.tcl \
)


HLS_IP_MUL_OUTS = $(IP_BUILD_DIR)/mul_hls/solution/impl/ip/hdl/verilog


# Usage: BUILD_VIVADO_HLS_IP
# Args: $1 = HLS_IP_NAME (High level synthesis IP name)
#       $2 = PART_ID (<device>/<package>/<speedgrade>)
#       $3 = HLS_IP_SRCS (Absolute paths to the HLS IP source files)
#       $4 = HLS_IP_SRC_DIR (Absolute path to the top level HLS IP src dir)
#       $5 = HLS_IP_BUILD_DIR (Absolute path to the top level HLS IP build dir)
#       $6 = HLS_IP_INCLUDES (Absolute path to IP include dir)
# Prereqs:
# - TOOLS_DIR must be defined globally
# -------------------------------------------------------------------

$(HLS_IP_MUL_OUTS) $(HLS_IP_MUL_SRCS) :
	$(call BUILD_VIVADO_HLS_IP,mul_hls,$(PART_ID),$(HLS_IP_MUL_SRCS),$(RFNOC_FPGA_DIR)/hls/,$(IP_BUILD_DIR),)
#                                     1        2             3                    4                     5 
