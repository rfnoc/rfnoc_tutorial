#include <complex>
#include "ap_int.h"

struct axis_cplx {
	std::complex<short int> data;
	ap_uint<1> last;	
};



void mul_hls(axis_cplx &in, axis_cplx &out) {

	#pragma HLS INTERFACE ap_ctrl_none port=return
	#pragma HLS INTERFACE axis port=in
	#pragma HLS INTERFACE axis port=out
	

	#pragma HLS DATA_PACK variable=in.data
	#pragma HLS DATA_PACK variable=out.data
	
	out.data.real() = in.data.real();
	out.data.imag() = in.data.imag();

	out.last = in.last;
}
