#include <random>
#include <stdio.h>
#include "hls_cmplx_mul_clip.h"

int16_t rand_shortint(int min, int max) {
	int32_t output = min + (rand() % static_cast<int>(max - min + 1));
	return output;
}

#define IN_MIN	-255
#define IN_MAX	255


int main() {
int16_t gains[] = {1, -1, 0, 30, -25, 256, 350, -350};
	srand(0);

	stream inputStream;
	stream outputStream;

	for (auto gain : gains) {
		printf("\nGain: %d\n", gain);
		for (int i = 0; i < 10; i++) {
			int16_t re = rand_shortint(IN_MIN, IN_MAX);
			int16_t im = rand_shortint(IN_MIN, IN_MAX);

			rfnoc_cmpl valIn(re, im);
			inputStream << valIn;
			printf("IN: %d, %d\n", valIn.real(), valIn.imag());
		}

		hls_multiplier(inputStream, gain, outputStream);
		for (int i = 0; i < 10; i++) {
			rfnoc_cmpl valOut;
			outputStream.read(valOut);

			printf("Out: %d, %d\n", valOut.real(), valOut.imag());
		}
	}

	return 0;
}
