#include <stdint.h>
#include <complex>
#include "ap_int.h"

struct axis_cplx {
	std::complex<int16_t> data;
	ap_uint<1> last;
};

void hls_cmplx_mul_clip(axis_cplx &inStream, int16_t gain, axis_cplx &outStream);
