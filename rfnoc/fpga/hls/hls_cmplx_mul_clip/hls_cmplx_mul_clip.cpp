

#include "hls_cmplx_mul_clip.h"

static int16_t clip(int32_t in) {
	if (in > 32767)
		return 32767;
	if (in < -32768)
		return -32768;
	return (int16_t)in;
}


void hls_cmplx_mul_clip(axis_cplx &inStream, int16_t gain, axis_cplx &outStream) {

#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis port=inStream
#pragma HLS INTERFACE axis port=outStream

#pragma HLS DATA_PACK variable=inStream.data
#pragma HLS DATA_PACK variable=outStream.data

	int16_t re = clip(inStream.data.real() * gain);
	int16_t im = clip(inStream.data.imag() * gain);
	outStream.data.real() = re;
	outStream.data.imag() = im;
	outStream.last = inStream.last;
}
