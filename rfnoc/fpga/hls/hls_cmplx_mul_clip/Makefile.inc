

# ADD C/C++/tcl files
include $(TOOLS_DIR)/make/viv_hls_ip_builder.mak

# NOTE Имя должно совпадать с названием папки с IP
HLS_IP_NAME = hls_cmplx_mul_clip


HLS_IP_MUL_CLIP_SRCS = $(addprefix $(IP_BUILD_DIR)/hls_cmplx_mul_clip/, \
hls_cmplx_mul_clip.cpp \
hls_cmplx_mul_clip.tcl \
hls_cmplx_mul_clip.h \
)

# NOTE Заставляем make пересобирать IP ядро при изменении исходных файлов, ставим в зависимости
HLS_IP_MUL_CLIP_LOCAL_SRCS = $(addprefix $(RFNOC_FPGA_DIR)/hls/hls_cmplx_mul_clip/, \
hls_cmplx_mul_clip.cpp \
hls_cmplx_mul_clip.tcl \
hls_cmplx_mul_clip.h \
)

# NOTE Основная цели для симуляции и сборки. Добавляется в Makefile.srcs верхнего уровня
HLS_IP_MUL_CLIP_OUTS = $(IP_BUILD_DIR)/hls_cmplx_mul_clip/solution/impl/ip/hdl/verilog


# Usage: BUILD_VIVADO_HLS_IP
# Args: $1 = HLS_IP_NAME (High level synthesis IP name)
#       $2 = PART_ID (<device>/<package>/<speedgrade>)
#       $3 = HLS_IP_SRCS (Absolute paths to the HLS IP source files)
#       $4 = HLS_IP_SRC_DIR (Absolute path to the top level HLS IP src dir)
#       $5 = HLS_IP_BUILD_DIR (Absolute path to the top level HLS IP build dir)
#       $6 = HLS_IP_INCLUDES (Absolute path to IP include dir)
# Prereqs:
# - TOOLS_DIR must be defined globally
# -------------------------------------------------------------------

$(HLS_IP_MUL_CLIP_OUTS) $(HLS_IP_MUL_CLIP_SRCS) : $(HLS_IP_MUL_CLIP_LOCAL_SRCS)
	$(call BUILD_VIVADO_HLS_IP,$(HLS_IP_NAME),$(PART_ID),$(HLS_IP_MUL_CLIP_SRCS),$(RFNOC_FPGA_DIR)/hls/,$(IP_BUILD_DIR),)
#                                 1                     2             3                    4                     5 
