#
# Copyright 2015 Ettus Research
# Copyright 2016 Ettus Research, a National Instruments Company
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#

include $(TOOLS_DIR)/make/viv_ip_builder.mak

LIB_IP_MULTIPLIER_SRCS = $(IP_BUILD_DIR)/multiplier/multiplier.xci

LIB_IP_MULTIPLIER_OUTS = $(addprefix $(IP_BUILD_DIR)/multiplier/, \
multiplier.xci.out \
synth/multiplier.vhd \
)

# RFNOC_FPGA_DIR задается при симуляции в rfnoc_block_gainip/Makefile
# при синтезе в ../../Makefile.srcs
$(LIB_IP_MULTIPLIER_SRCS) $(LIB_IP_MULTIPLIER_OUTS) : $(RFNOC_FPGA_DIR)/ip/multiplier/multiplier.xci
	$(call BUILD_VIVADO_IP,multiplier,$(ARCH),$(PART_ID),$(RFNOC_FPGA_DIR)/ip,$(IP_BUILD_DIR),0)

