# General parameters
# -----------------------------------------
schema: rfnoc_imagebuilder_args         # Identifier for the schema used to validate this file
copyright: 'Ettus Research, A National Instruments Brand' # Copyright information used in file headers
license: 'SPDX-License-Identifier: LGPL-3.0-or-later' # License information used in file headers
version: 1.0                            # File version
rfnoc_version: 1.0                      # RFNoC protocol version
chdr_width: 64                          # Bit width of the CHDR bus for this image
device: 'n310'
default_target: 'N310_HG'

# A list of all stream endpoints in design
# ----------------------------------------
stream_endpoints:
  ep0:                       # Stream endpoint name
    ctrl: True                      # Endpoint passes control traffic
    data: True                      # Endpoint passes data traffic
    buff_size: 32768                # Ingress buffer size for data
  ep1:                       # Stream endpoint name
    ctrl: False                     # Endpoint passes control traffic
    data: True                      # Endpoint passes data traffic
    buff_size: 32768                # Ingress buffer size for data
  ep2:                       # Stream endpoint name
    ctrl: False                     # Endpoint passes control traffic
    data: True                      # Endpoint passes data traffic
    buff_size: 32768                # Ingress buffer size for data
  ep3:                       # Stream endpoint name
    ctrl: False                     # Endpoint passes control traffic
    data: True                      # Endpoint passes data traffic
    buff_size: 32768                # Ingress buffer size for data
  ep4:                       # Stream endpoint name
    ctrl: False                     # Endpoint passes control traffic
    data: True                      # Endpoint passes data traffic
    buff_size: 32768                # Ingress buffer size for data

# A list of all NoC blocks in design
# ----------------------------------
noc_blocks:
  duc0:                      # NoC block name
    block_desc: 'duc.yml'    # Block device descriptor file
    parameters:
      NUM_PORTS: 2
  ddc0:
    block_desc: 'ddc.yml'
    parameters:
      NUM_PORTS: 2
  radio0:
    block_desc: 'radio_2x64.yml'
  duc1:
    block_desc: 'duc.yml'
    parameters:
      NUM_PORTS: 2
  ddc1:
    block_desc: 'ddc.yml'
    parameters:
      NUM_PORTS: 2
  radio1:
    block_desc: 'radio_2x64.yml'
  gainip0:
    block_desc: 'gainhls.yml'

# A list of all static connections in design
# ------------------------------------------
# Format: A list of connection maps (list of key-value pairs) with the following keys
#         - srcblk  = Source block to connect
#         - srcport = Port on the source block to connect
#         - dstblk  = Destination block to connect
#         - dstport = Port on the destination block to connect
connections:
  # ep0 -> duc0_0 -> radio0_0: TX
  - { srcblk: ep0,    srcport: out0,  dstblk: duc0,   dstport: in_0 }
  - { srcblk: duc0,   srcport: out_0, dstblk: radio0, dstport: in_0 }
  
  # radio0_0 -> ddc0_0 -> ep0: RX 
  - { srcblk: radio0, srcport: out_0, dstblk: ddc0,   dstport: in_0 }
  - { srcblk: ddc0,   srcport: out_0, dstblk: ep0,    dstport: in0  }
  
  # ep1 -> duc0_1 -> radio0_1: TX
  - { srcblk: ep1,    srcport: out0,  dstblk: duc0,   dstport: in_1 }
  - { srcblk: duc0,   srcport: out_1, dstblk: radio0, dstport: in_1 }
  
  # radio0_1 -> ddc0_1 -> ep1: RX
  - { srcblk: radio0, srcport: out_1, dstblk: ddc0,   dstport: in_1 }
  - { srcblk: ddc0,   srcport: out_1, dstblk: ep1,    dstport: in0  }
  
  # ep2 -> duc1_0 -> radio1_0: TX
  - { srcblk: ep2,    srcport: out0,  dstblk: duc1,   dstport: in_0 }
  - { srcblk: duc1,   srcport: out_0, dstblk: radio1, dstport: in_0 }
  
  # radio1_0 -> ddc1_0 -> ep2: RX
  - { srcblk: radio1, srcport: out_0, dstblk: ddc1,   dstport: in_0 }
  - { srcblk: ddc1,   srcport: out_0, dstblk: ep2,    dstport: in0  }
  
  # ep3 -> duc1_1 -> radio1_1: TX
  - { srcblk: ep3,    srcport: out0,  dstblk: duc1,   dstport: in_1 }
  - { srcblk: duc1,   srcport: out_1, dstblk: radio1, dstport: in_1 }
  
  # radio1_1 -> ddc1_1 -> ep3: RX
  - { srcblk: radio1, srcport: out_1, dstblk: ddc1,   dstport: in_1 }
  - { srcblk: ddc1,   srcport: out_1, dstblk: ep3,    dstport: in0  }
  
  # ep4 -> gainip0
  - { srcblk: ep4,    srcport: out0,  dstblk: gainip0,  dstport: in   }
  # gainip0 -> ep4
  - { srcblk: gainip0,  srcport: out,   dstblk: ep4,    dstport: in0  }
  
  - { srcblk: radio0, srcport: ctrl_port, dstblk: _device_, dstport: ctrlport_radio0 }
  - { srcblk: radio1, srcport: ctrl_port, dstblk: _device_, dstport: ctrlport_radio1 }
  - { srcblk: _device_, srcport: x300_radio0, dstblk: radio0, dstport: x300_radio }
  - { srcblk: _device_, srcport: x300_radio1, dstblk: radio1, dstport: x300_radio }
  - { srcblk: _device_, srcport: time_keeper, dstblk: radio0, dstport: time_keeper }
  - { srcblk: _device_, srcport: time_keeper, dstblk: radio1, dstport: time_keeper }

# A list of all clock domain connections in design
# ------------------------------------------------
# Format: A list of connection maps (list of key-value pairs) with the following keys
#         - srcblk  = Source block to connect (Always "_device"_)
#         - srcport = Clock domain on the source block to connect
#         - dstblk  = Destination block to connect
#         - dstport = Clock domain on the destination block to connect
clk_domains:
  - { srcblk: _device_, srcport: radio,      dstblk: radio0, dstport: radio }
  - { srcblk: _device_, srcport: rfnoc_chdr, dstblk: ddc0,   dstport: ce    }
  - { srcblk: _device_, srcport: rfnoc_chdr, dstblk: duc0,   dstport: ce    }
  - { srcblk: _device_, srcport: radio,      dstblk: radio1, dstport: radio }
  - { srcblk: _device_, srcport: rfnoc_chdr, dstblk: ddc1,   dstport: ce    }
  - { srcblk: _device_, srcport: rfnoc_chdr, dstblk: duc1,   dstport: ce    }
